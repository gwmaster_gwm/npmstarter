import React, { useEffect, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { ExampleWorkerActions } from './ExampleWorker.action'

function ExampleWorker (props) {
  // action
  const {
    runWorker,
    showValueOnAll,
    showValueOnWorker,
    showValueOnMain
  } = props
  // reducer
  const {exampleWorkerValue} = props

  return (<div>
    exampleWorkerValue : {exampleWorkerValue}
    <br/>
    <button onClick={() => runWorker()} >Run Worker</button>
    <button onClick={() => showValueOnAll()} >showValueOnAll</button>
    <button onClick={() => showValueOnWorker()} >showValueOnWorker</button>
    <button onClick={() => showValueOnMain()} >showValueOnMain</button>
  </div>)
}

function mapActionsToProps (dispatch) {
  return bindActionCreators(
    {
      ...ExampleWorkerActions
    }
    , dispatch)
}

function mapStoreToProps ({ exampleWorkerValue }, ownProps) {
  return {
    exampleWorkerValue
  }
}

ExampleWorker.propTypes = {}
ExampleWorker.defaultProps = {}

export default connect(mapStoreToProps, mapActionsToProps)(ExampleWorker)