import { EXAMPLE_WORKER } from './ExampleWorker.const'
import { WORKERS } from '../../../../npm/redux-saga-worker/src/worker.const'

const runWorker = payload => ({ type: EXAMPLE_WORKER.RUN_WORKER_SAGA, payload, sendTo: WORKERS.WORKER })
const showValueOnAll = payload => ({ type: EXAMPLE_WORKER.SHOW_CURRENT_REDUX_VALUE_SAGA, payload, sendTo: WORKERS.ALL })
const showValueOnWorker = payload => ({ type: EXAMPLE_WORKER.SHOW_CURRENT_REDUX_VALUE_SAGA, payload, sendTo: WORKERS.WORKER })
const showValueOnMain = payload => ({ type: EXAMPLE_WORKER.SHOW_CURRENT_REDUX_VALUE_SAGA, payload, sendTo: WORKERS.MAIN })


export const ExampleWorkerActions = {
  runWorker,
  showValueOnAll,
  showValueOnWorker,
  showValueOnMain
}