import { all, call, put, select, takeLatest } from 'redux-saga/effects'
import { EXAMPLE_WORKER } from './ExampleWorker.const'
import { WORKERS } from 'redux-saga-worker'

function * runWorkerSaga() {
  let {
    exampleWorkerValue
  } = yield select(state => state)
  yield put({ type: EXAMPLE_WORKER.SET_WORKER_VALUE, payload: ++exampleWorkerValue , sendTo: WORKERS.ALL })
  console.log(exampleWorkerValue)
}
function * showCurrentValue(payload) {
  let {
    exampleWorkerValue
  } = yield select(state => state)
  const {receiver,sendTo} = payload
  console.log({receiver , sendTo , exampleWorkerValue})
}



export default function * () {
  yield takeLatest(EXAMPLE_WORKER.RUN_WORKER_SAGA, runWorkerSaga)
  yield takeLatest(EXAMPLE_WORKER.SHOW_CURRENT_REDUX_VALUE_SAGA , showCurrentValue)
}
