import { EXAMPLE } from './Example.const'

//
function exampleValue2 (state = 0, { type, payload }) {
  if (type === EXAMPLE.SET_VALUE) {
    return payload
  }
  return state
}

export default {
  exampleValue : [EXAMPLE.SET_VALUE , 0],
  exampleValue2
}
