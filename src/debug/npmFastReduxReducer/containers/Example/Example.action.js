import { EXAMPLE } from './Example.const'
const setValue = payload => ({ type: EXAMPLE.SET_VALUE, payload })
export const ExampleActions = {
  setValue
}