import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ExampleActions } from './Example.action'
import { mapActionsToProps, mapStoreToProps } from 'fast-redux-reducer'

function Example (props) {
  // reducer
  const {exampleValue,exampleValue2} = props
  // actions
  const {setValue} = props
  return(
    <div>
      <div>
        ExampleValue  : {exampleValue} <br/>
        ExampleValue2 : {exampleValue}
      </div>
      <button onClick={() => setValue(Math.random(100)*100)} >Set</button>
    </div>
  )
}

export default  connect(
  mapStoreToProps('exampleValue','exampleValue2'),
  mapActionsToProps(ExampleActions)
)(Example)