import {autoReducer} from 'fast-redux-reducer'
import ExampleReducer from '../Example/Example.reducer'
export const reducersObject = {
  ExampleReducer
}
export default autoReducer(reducersObject)