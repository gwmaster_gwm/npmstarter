import React from 'react'
import ReactDOM from 'react-dom'
import { combineReducers, createStore } from 'redux'
import { Provider } from 'react-redux'
import reducers from './containers/store/reducers'
import Example from './containers/Example/Example'

function App() {
  let combinedReducers = combineReducers(reducers)
  const store = createStore(combinedReducers)

  return (
    <Provider store={store}>
      <Example/>
    </Provider>
  );
}

ReactDOM.render(<App/>, document.getElementById('app'))
