import myReducers from './reducers'
import mySaga from './sagas'
import {workerStore} from '../../../../npm/redux-saga-worker/src/'

// logger
import { createLogger } from 'redux-logger'
const logger = createLogger({
  collapsed: true,
  colors: {
    title: () => '#2004f4',
    prevState: () => '#9E9E9E',
    action: () => '#03A9F4',
    nextState: () => '#4CAF50',
    error: () => '#F20404'
  }
})

const onInit = (initParams) => {
  console.log("Warker Iniylized")
  console.log(initParams)
}
const config = {
  saga : mySaga,
  reducers : myReducers,
  onInit,
  middleware : [],
  serialize : (data) => {return JSON.stringify(data)},
  deserialize : (data) => {return JSON.parse(data)}
}
workerStore(config)






