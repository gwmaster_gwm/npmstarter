import React, { useEffect, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { ExampleWorkerActions } from './ExampleWorker.action'
import { WORKERS } from '../../../../npm/redux-saga-worker/src'

function ExampleWorker (props) {
  // action
  const {
    runWorker,
    showValueOnAll,
    showValueOnWorker,
    showValueOnMain
  } = props
  // reducer
  const {exampleWorkerValue} = props

  return (<div>
    exampleWorkerValue : {exampleWorkerValue}
    <br/>
    <button onClick={() => runWorker(WORKERS.ALL)} >Run Worker send back to all</button>
    <button onClick={() => runWorker('worker2')} >Run Worker send back to worker 2</button>
    <button onClick={() => runWorker(WORKERS.MAIN)} >Run Worker send back to main</button>
    <br/>
    <button onClick={() => showValueOnAll()} >showValueOnAll</button>
    <br/>
    <button onClick={() => showValueOnWorker()} >showValueOnWorker</button>
    <button onClick={() => showValueOnWorker('worker2')} >showValueOnWorker : worker2</button>
    <button onClick={() => showValueOnMain()} >showValueOnMain</button>
  </div>)
}

function mapActionsToProps (dispatch) {
  return bindActionCreators(
    {
      ...ExampleWorkerActions
    }
    , dispatch)
}

function mapStoreToProps ({ exampleWorkerValue }, ownProps) {
  return {
    exampleWorkerValue
  }
}

ExampleWorker.propTypes = {}
ExampleWorker.defaultProps = {}

export default connect(mapStoreToProps, mapActionsToProps)(ExampleWorker)