import { EXAMPLE_WORKER } from './ExampleWorker.const'
import { WORKERS } from '../../../../npm/redux-saga-worker/src/worker.const'

const runWorker = (payload,sendTo = WORKERS.WORKER) => ({ type: EXAMPLE_WORKER.RUN_WORKER_SAGA, payload,  sendTo })
const showValueOnAll = payload => ({ type: EXAMPLE_WORKER.SHOW_CURRENT_REDUX_VALUE_SAGA, payload, sendTo: WORKERS.ALL })
const showValueOnWorker = (sendTo = WORKERS.WORKER) => ({ type: EXAMPLE_WORKER.SHOW_CURRENT_REDUX_VALUE_SAGA,  sendTo })
const showValueOnMain = payload => ({ type: EXAMPLE_WORKER.SHOW_CURRENT_REDUX_VALUE_SAGA, payload })


export const ExampleWorkerActions = {
  runWorker,
  showValueOnAll,
  showValueOnWorker,
  showValueOnMain
}