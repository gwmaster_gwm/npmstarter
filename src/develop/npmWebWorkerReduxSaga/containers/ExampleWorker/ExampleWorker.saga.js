import { all, call, put, select, takeLatest , delay } from 'redux-saga/effects'
import { EXAMPLE_WORKER } from './ExampleWorker.const'
import { WORKERS } from '../../../../npm/redux-saga-worker/src/worker.const'

function * runWorkerSaga({payload : sendBackTo }) {
  let {
    exampleWorkerValue
  } = yield select(state => state)
  let newValue = exampleWorkerValue+1;
  console.log('runWorkerSaga')

  yield put({ type: EXAMPLE_WORKER.SET_WORKER_VALUE, payload : newValue, sendTo: sendBackTo })
  //yield put({ type: EXAMPLE_WORKER.SET_WORKER_VALUE, payload: newValue , sendTo: WORKERS.ALL })
  //yield put({ type: EXAMPLE_WORKER.SET_WORKER_VALUE, payload : newValue, sendTo: 'worker2' })
  //yield put({ type: EXAMPLE_WORKER.SET_WORKER_VALUE, payload : newValue })
}
function * showCurrentValue(event) {
  let {
    exampleWorkerValue
  } = yield select(state => state)
  const {receiver,sendTo} = event
  console.log(`showCurrentValue sendTo : ${sendTo}   receiver : ${receiver}  exampleWorkerValue :  ${exampleWorkerValue}`)
}

function *  onSetValue(event) {
  const {receiver,sendTo , payload} = event
  console.log(`onSetValue sendTo : ${sendTo}   receiver : ${receiver}  payload :  ${payload}`)
}


export default function * () {
  yield takeLatest(EXAMPLE_WORKER.RUN_WORKER_SAGA, runWorkerSaga)
  yield takeLatest(EXAMPLE_WORKER.SHOW_CURRENT_REDUX_VALUE_SAGA , showCurrentValue)
  yield takeLatest(EXAMPLE_WORKER.SET_WORKER_VALUE, onSetValue )
}
