import React from 'react'
import ReactDOM from 'react-dom'
import createSagaMiddleware from 'redux-saga'
import { combineReducers, applyMiddleware ,  createStore } from 'redux'
import { Provider } from 'react-redux'
import myReducers from './containers/store/reducers'
import mySaga from './containers/store/sagas'
import {toWorkerMiddleware} from '../../npm/redux-saga-worker/src'
import ExampleWorker from './containers/ExampleWorker/ExampleWorker'

// logger
import { createLogger } from 'redux-logger'
const logger = createLogger({
  collapsed: true,
  colors: {
    title: () => '#a99c7c',
    prevState: () => '#9E9E9E',
    action: () => '#03A9F4',
    nextState: () => '#4CAF50',
    error: () => '#F20404'
  }
})

// create worker
import StoreWorker from './containers/storeWorker/store.worker'
import StoreWorker2 from './containers/storeWorker2/store.worker'

function App() {
  const sagaMiddleware = createSagaMiddleware()
  const [workerMiddleware,initWorkerMiddleware] = new toWorkerMiddleware()
  const [workerMiddleware2,initWorkerMiddleware2] = new toWorkerMiddleware()


  let combinedReducers = combineReducers(myReducers)
  const middleware = [
    workerMiddleware, // must be first
    workerMiddleware2,
    sagaMiddleware,
  ]
  const store = createStore(
    combinedReducers,
    applyMiddleware(...middleware)
  )
  // then run the saga
  sagaMiddleware.run(mySaga)
  // then init worker
  const onInit = (data) => {
    console.log("Main Iniylized ")
    console.log(data)
  }
  const config = {
    store,
    storeWorker : new StoreWorker(),
    initConfig : {
      exampleParam : 'exampleValue'
    },
    onInit,
    serialize : (data) => { return JSON.stringify(data)},
    deserialize : (data) => {return JSON.parse(data)}
  }
    initWorkerMiddleware(config)


  const config2 = {
    store,
    storeWorker : new StoreWorker2(),
    workerName : 'worker2',
    onInit,
  }
  initWorkerMiddleware2(config2)

  return (
    <Provider store={store}>
      <ExampleWorker/>
    </Provider>
  );
}

ReactDOM.render(<App/>, document.getElementById('app'))
