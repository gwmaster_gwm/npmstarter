import React from 'react'
import ReactDOM from 'react-dom'
import {Card , Box} from '../../npm/npm-starter-example/src/index'
//import Card from './components/Card'

function App() {
  return (
    <div>
      <Card>Develop</Card>
      <Box>Develop</Box>
    </div>
  );
}

ReactDOM.render(<App/>, document.getElementById('app'))
